import sys
import json
from typing import List


class Analyser:
    def __init__(self, sample_name_col: str, target_name_col: str, ct_value_col: str, control_gene: str,
                 groups, control_groups):
        self.sample_name_col = sample_name_col
        self.target_name_col = target_name_col
        self.ct_value_col = ct_value_col
        self.control_gene = control_gene
        self.groups = groups
        self.control_groups = control_groups

        self.target_names = [control_gene]
        self.samples = dict()
        self.results = dict()
        self.error_map = dict()

    def import_data(self, filename: str):
        # Extract from header the indices of the columns we need.
        f = open(filename, 'r')
        file_header = f.readline().split(',')
        sample_name_idx = self.__get_column_idx(self.sample_name_col, file_header)
        target_name_idx = self.__get_column_idx(self.target_name_col, file_header)
        ct_value_idx = self.__get_column_idx(self.ct_value_col, file_header)
        min_cols_expected = max([sample_name_idx, target_name_idx, ct_value_idx]) + 1
        # Accumulate data by sample name
        line_count = 1
        for line in f.readlines():
            line_count += 1
            cells = line.split(',')
            if len(cells) < min_cols_expected:
                print(f'Error reading line {line_count}, expected at least {min_cols_expected} columns')
                continue
            sample_name = cells[sample_name_idx]
            if not self.__include_sample(sample_name):
                print(f'Skipping sample {sample_name} because it doesn\'t belong to any group')
                continue
            target_name = cells[target_name_idx]
            ct_value = cells[ct_value_idx]

            self.samples.setdefault(sample_name, dict())
            try:
                self.samples[sample_name][target_name] = float(ct_value)
                if target_name not in self.target_names:
                    self.target_names.append(target_name)
            except Exception as e:
                self.__report_error_for_sample(
                    sample_name, f'Failed to read ct-value from line {line_count}, error: {e}')

    def analyse(self, output_file: str):
        f_out = open(output_file, 'w')
        f_out.write(self.__create_header() + '\n')
        for group_name, group in self.groups.items():
            for sample_name in group:
                control_group = self.groups[self.control_groups[group_name]]
                info = self.__calc_sample_info(sample_name, control_group)
                if info:
                    f_out.write(info + '\n')
            f_out.write('\n')
        f_out.close()

    def get_result(self, sample_name: str, target: str):
        return self.results.get((sample_name, target), None)

    def get_errors(self, sample_name: str):
        return self.error_map.get(sample_name, None)

    def report_errors(self):
        sample_names = list(self.error_map.keys())
        sample_names.sort()
        if sample_names:
            print('The following issues were reported:')
        for sample_name in sample_names:
            if sample_name in self.error_map:
                errors = self.error_map[sample_name]
                print(f'Errors reported on sample "{sample_name}":')
                for error in errors:
                    print(f'\t{error}')
            print('\n')

    def __get_column_idx(self, column_header: str, columns: List[str]):
        if column_header not in columns:
            raise RuntimeError(f'No column name \'{column_header}\' was found')
        return columns.index(column_header)

    def __create_header(self):
        header = 'sample_name'
        for target_name in self.target_names:
            header += ',' + target_name
            if (target_name != self.control_gene):
                header += ',dCT,ddCT,RQ'
        return header

    def __calc_group_avg_ddct(self, group: List[str], target_name: str):
        if not group:
            return None
        total, count = 0, 0
        for sample_name in group:
            if sample_name not in self.samples:
                continue    # skip sample if not found
            sample_data = self.samples[sample_name]
            if target_name not in sample_data:
                continue
            if self.control_gene not in sample_data:
                continue
            dCT = sample_data[target_name] - sample_data[self.control_gene]
            ddCT = 2**(-dCT)
            total += ddCT
            count += 1
        if count == 0:
            raise RuntimeError('Cannot calculate average ddct for group because it has zero samples')
        return total / count

    def __calc_sample_info(self, sample_name: str, control_group: List[str]):
        if sample_name not in self.samples:
            self.__report_error_for_sample(sample_name, f'Error: sample {sample_name} was not found')
            return ''

        sample_data = self.samples[sample_name]
        info = sample_name + ','
        if self.control_gene not in sample_data:
            self.__report_error_for_sample(
                sample_name,
                f'Error: sample is missing control gene {self.control_gene}, will add empty row to the results')
            return info

        for target_name in self.target_names:
            if target_name not in sample_data:  # ct-value for this gene not found, leave empty
                self.__report_error_for_sample(
                    sample_name,
                    f'Error: sample is missing target {target_name}')
                info += ' , , , ,'
                continue
            else:
                info += f'{sample_data[target_name]},'
                if target_name != self.control_gene:
                    dCT = sample_data[target_name] - sample_data[self.control_gene]
                    ddCT = 2**(-dCT)
                    RQ = None
                    try:
                        control_group_avg_ddCT = self.__calc_group_avg_ddct(control_group, target_name)
                        RQ = ddCT / control_group_avg_ddCT
                        info += f'{dCT:.8f},{ddCT:.8f},{RQ:.8f},'
                    except Exception as e:
                        self.__report_error_for_sample(
                            sample_name,
                            f'Error: failed to calculate RQ value for target {target_name}, '
                            f'encountered error: {e}')
                        info += f'{dCT:.8f},{ddCT:.8f},CALC_ERROR,'
                    self.results[(sample_name, target_name)] = (dCT, ddCT, RQ)
        return info

    def __include_sample(self, sample_name: str):
        for group in self.groups.values():
            if sample_name in group:
                return True
        return False

    def __report_error_for_sample(self, sample_name: str, error_msg: str):
        errors = self.error_map.setdefault(sample_name, list())
        errors.append(error_msg)


def create_analyser(config):
    return Analyser(
        config['sample_name_col'],
        config['target_name_col'],
        config['ct_value_col'],
        config['control_gene'],
        config['groups'],
        config['control_groups'])


def main():
    if len(sys.argv) != 4:
        print('Usage: python3 analyse.py <config_file> <input_file> <output_file>')
        return

    analyser = None
    try:
        config = json.load(open(sys.argv[1], 'r'))
        analyser = create_analyser(config)
        analyser.import_data(sys.argv[2])
        analyser.analyse(output_file=sys.argv[3])
        analyser.report_errors()
    except Exception as e:
        analyser.report_errors()
        print(f'Program did not complete, could not recover after encountering error: {e}')


if __name__ == '__main__':
    main()
