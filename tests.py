import unittest
import json
from analyser import create_analyser


def init_analyser(config_path, data_path):
    config = json.load(open(config_path, 'r'))
    analyser = create_analyser(config)
    analyser.import_data(data_path)
    return analyser


class AnalyserTests(unittest.TestCase):
    def __init__(self, *args, **kwargs):
        unittest.TestCase.__init__(self, *args, **kwargs)
        self.analyser = init_analyser('test_data/example_config.json', 'test_data/example_data.csv')
        self.analyser.analyse('tmp_out.csv')

    def test_VerifySampleOutsideCtrlGroupHasCorrectValues(self):
        # Checking some random samples have the expected values
        # Expected values have already been verified.
        x = self.analyser.get_result('F05_AA', 'gene1')
        self.assertAlmostEqual(x[0], 3.30800000)
        self.assertAlmostEqual(x[1], 0.10097010)
        self.assertAlmostEqual(x[2], 0.74276010)

        x = self.analyser.get_result('F03_BB', 'gene1')
        self.assertAlmostEqual(x[0], 4.67200000)
        self.assertAlmostEqual(x[1], 0.03922725)
        self.assertAlmostEqual(x[2], 0.28856500)

        x = self.analyser.get_result('F04_BB', 'gene2')
        self.assertAlmostEqual(x[0], 8.54500000)
        self.assertAlmostEqual(x[1], 0.00267731)
        self.assertAlmostEqual(x[2], 0.11850347)

        x = self.analyser.get_result('F06_CC', 'gene2')
        self.assertAlmostEqual(x[0], 5.57700000)
        self.assertAlmostEqual(x[1], 0.02094863)
        self.assertAlmostEqual(x[2], 0.92723133)

    def test_TargetOmittedIfItHasBadCTValue(self):
        x1 = self.analyser.get_result('F02_BB', 'gene1')
        assert x1 is None

    def test_BadValueDoesNotAffectCalculationOfOtherTargetsWithinSameSample(self):
        x1 = self.analyser.get_result('F02_BB', 'gene1')
        assert x1 is None
        x2 = self.analyser.get_result('F02_BB', 'gene2')
        self.assertAlmostEqual(x2[0], 7.81400000)
        self.assertAlmostEqual(x2[1], 0.00444377)
        self.assertAlmostEqual(x2[2], 0.19669078)

    def test_SampleOmittedWhenItHasUndefinedValueForCtrlGene(self):
        self.assertIsNone(self.analyser.get_result('F03_CC', 'gene1'))
        self.assertIsNone(self.analyser.get_result('F03_CC', 'gene2'))
        self.assertTrue(self.analyser.get_errors('F03_CC'))


if __name__ == '__main__':
    unittest.main()
